// JavaScript
document.addEventListener("DOMContentLoaded", function() {
    var dynamicWords = ["la pasión", "la emoción", "la vuelta"];
    var dynamicWordElement = document.getElementById("dynamic-word");
    var index = 0;

    setInterval(function() {
        dynamicWordElement.textContent = dynamicWords[index];
        index = (index + 1) % dynamicWords.length;
    }, 2000); // Cambia la palabra cada 2 segundos (2000 milisegundos)
});
