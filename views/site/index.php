<?php
/*  */
/** @var yii\web\View $this */
use yii\helpers\Url;

$this->title = 'My Yii Application';
?>

<?php 
// Registra el archivo JavaScript
$this->registerJsFile('@web/js/prueba.js', ['depends' => [\yii\web\JqueryAsset::class]]);
?>


<div class="site-index">

    
    <div class="jumbotron text-color text-center" style="background-image: url('https://images.pexels.com/photos/2876511/pexels-photo-2876511.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1'); background-size: cover; background-position: center;">
       
    </div>

        <!-- Introducción breve -->
    <div class="intro-section text-center">
        <h2>Revive <span id="dynamic-word"></span></h2>
        <p>Sumérgete en la emocionante historia de la Vuelta Ciclista a España a través de nuestra aplicación. Revive momentos legendarios, descubre datos históricos y explora la pasión por el ciclismo en cada edición. La Retrovuelta te invita a explorar el pasado de esta icónica carrera y a compartir la emoción por el ciclismo. ¡Únete a nosotros en este viaje por la historia de la Vuelta!</p>
    </div>
        
        
        

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="./images/image2.jpg" alt="Image 1">
            </div>  
            <div class="carousel-item">
                <img src="./images/85E_2866.jpg" alt="Image 2">
            </div>
            <div class="carousel-item">
                <img src="./images/image3.jpg" alt="Image 3">
            </div>
        </div>
    </div>  

   
    
<div class="card-deck text-center mt-2">
    <style>
        .card-body:hover {
    background-color: #AD3333 !important;
}
    </style>
    <div class="card mx-2">
        <a href="<?= Url::to('site/puertos') ?>" class="card-body btn btn" data-title="Puertos">
            <img src="<?= Yii::getAlias('@web') ?>/images/Montaña.png" alt="Etapas">
        </a>
    </div>
    <div class="card mx-2">
        <a href="<?= Url::to(['site/etapas']) ?>" class="card-body btn btn-sm" data-title="Maillot Amarillo">
            <img src="<?= Yii::getAlias('@web') ?>/images/Maillot.png" alt="Puertos">
        </a>
    </div>
    <div class="card mx-2">
        <a href="<?= Url::to(['site/equipos']) ?>" class="card-body btn btn-sm" data-title="Equipos">
            <img src="<?= Yii::getAlias('@web') ?>/images/ciclista.png" alt="Equipos">
        </a>
    </div>
</div>
    
        
 


    
</div>