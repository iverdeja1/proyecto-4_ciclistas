
<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\bootstrap\BootstrapAsset;
use yii\widgets\LinkPager;

// Definición del título
$this->title = 'Los ciclistas';

?>


<div class="body-content mt-5">
    <style>
            .body-content {
          margin-left: 80px;
      }

      /* Ajuste para evitar que el margen afecte al header y al footer */
      header, footer {
          margin-left: 0px;
          width: calc(100% + 80px);
      }
    </style>
    <div class="row">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_ciclistas',
        'layout' => "{items}",
        'options' => ['class' => 'row'], // Clase de Bootstrap
        'itemOptions' => ['class' => 'col-sm-4 mb-3'], // Clase de Bootstrap para cada elemento
    ]); ?>
    </div>
</div>
<div class="row">
        <div class="col-sm-12 text-center"> <!-- Centra la paginación -->
            <?= LinkPager::widget([
                'pagination' => $dataProvider->pagination,
            ]); ?>
        </div>
    </div>
