<?php

    use yii\helpers\Html;
    use dosamigos\highcharts\HighCharts;
    
    //MGE
    $dataMGE = [];
    foreach ($mgeData as $maillot) {
        $dataMGE[] = [
            'name' => $maillot['nombre'],
            'y' => (float) $maillot['llevado'],
        ];
    }
    
    $chartMGE = [
        'chart' => ['type' => 'pie'],
        'title' => ['text' => 'Cantidad de maillots MGE ganados por ciclista'],
        'subtitle' => ['text' => 'El maillot amarillo es llevado por el ciclista líder en la clasificación general de una carrera por etapas, como el Tour de Francia. Simboliza el liderazgo y la excelencia en la competición.'],
        'series' => [['name' => 'Veces llevado', 'data' => $dataMGE]],
    ];
    
    //MMO
    $dataMMO = [];
    foreach ($mmoData as $maillot) {
        $dataMMO[] = [
            'name' => $maillot['nombre'],
            'y' => (float) $maillot['llevado'],
        ];
    }
    
    $chartMMO = [
        'chart' => ['type' => 'pie'],
        'title' => ['text' => 'Cantidad de maillots MMO ganados por ciclista'],
        'subtitle' => ['text' => 'El maillot blanco con lunares rojos, llamado "maillot de la montaña", es usado por el líder de la clasificación de montaña. Este ciclista acumula más puntos al pasar por las cimas de montaña categorizadas en la ruta.'],
        'series' => [['name' => 'Veces llevado', 'data' => $dataMMO]],
    ];
    
    //MMV
    $dataMMV = [];
    foreach ($mmvData as $maillot) {
        $dataMMV[] = [
            'name' => $maillot['nombre'],
            'y' => (float) $maillot['llevado'],
        ];
    }
    
    $chartMMV = [
        'chart' => ['type' => 'pie'],
        'title' => ['text' => 'Cantidad de maillots MMV ganados por ciclista'],
        'subtitle' => ['text' => 'El dorsal rojo es llevado por el ciclista que ha ganado más metas volantes durante la etapa de una carrera por etapas. Simboliza la habilidad del ciclista para dominar los puntos intermedios de sprint a lo largo del recorrido.'],
        'series' => [['name' => 'Veces llevado', 'data' => $dataMMV]],
    ];
    
    //MRE
    $dataMRE = [];
    foreach ($mreData as $maillot) {
        $dataMRE[] = [
            'name' => $maillot['nombre'],
            'y' => (float) $maillot['llevado'],
        ];
    }
    
    $chartMRE = [
        'chart' => ['type' => 'pie'],
        'title' => ['text' => 'Cantidad de maillots MRE ganados por ciclista'],
        'subtitle' => ['text' => 'El dorsal verde es llevado por el ciclista que ha sido más regular en una carrera por etapas. Simboliza la consistencia y el rendimiento constante a lo largo de las etapas de la competición.'],
        'series' => [['name' => 'Veces llevado', 'data' => $dataMRE]],
    ];
    
    //MSE
    $dataMSE = [];
    foreach ($mseData as $maillot) {
        $dataMSE[] = [
            'name' => $maillot['nombre'],
            'y' => (float) $maillot['llevado'],
        ];
    }
    
    $chartMSE = [
        'chart' => ['type' => 'pie'],
        'title' => ['text' => 'Cantidad de maillots MSE ganados por ciclista'],
        'subtitle' => ['text' => 'El dorsal rosa es llevado por el ciclista que ha ganado el sprint especial en una carrera por etapas. Simboliza la destreza en los sprints y la habilidad para obtener puntos en estos tramos específicos.'],
        'series' => [['name' => 'Veces llevado', 'data' => $dataMSE]],
    ];
    
    //MMS
    $dataMMS = [];
    foreach ($mmsData as $maillot) {
        $dataMMS[] = [
            'name' => $maillot['nombre'],
            'y' => (float) $maillot['llevado'],
        ];
    }
    
    $chartMMS = [
        'chart' => ['type' => 'pie'],
        'title' => ['text' => 'Cantidad de maillots MMS ganados por ciclista'],
        'subtitle' => ['text' => 'El dorsal de estrellas moradas es llevado por el ciclista que más se ha esforzado en la etapa de una carrera por etapas. Simboliza el sacrificio y la dedicación del ciclista en la competición diaria.'],
        'series' => [['name' => 'Veces llevado', 'data' => $dataMMS]],
    ];
    
    
    
 ?>   

<div class="site-index">
    <div class="container">
        <div class="row justify-content-center" style="margin-top: 64px !important;">
            <div class="col-md-5">
                <?= HighCharts::widget(['clientOptions' => $chartMGE]); ?>
            </div>
            <div class="col-md-5">
                <?= HighCharts::widget(['clientOptions' => $chartMMO]); ?>
            </div>
        </div>
        
        <div class="row justify-content-center" style="margin-top: 64px !important;">
            <div class="col-md-5">
                <?= HighCharts::widget(['clientOptions' => $chartMMV]); ?>
            </div>
            <div class="col-md-5">
                <?= HighCharts::widget(['clientOptions' => $chartMRE]); ?>
            </div>
        </div>
        
        <div class="row justify-content-center" style="margin-top: 64px !important;">
            <div class="col-md-5">
                <?= HighCharts::widget(['clientOptions' => $chartMSE]); ?>
            </div>
            <div class="col-md-5">
                <?= HighCharts::widget(['clientOptions' => $chartMMS]); ?>
            </div>
        </div>
    </div>
        
</div>