<?php

    use yii\helpers\Html;
    use dosamigos\highcharts\HighCharts;

    $this->title = $equipo;
    
    
    $data_victorias = [];
    $total_victorias = 0;
    
    foreach ($etapas as $etapa) {
        if (in_array($etapa, $victorias)) {
            $total_victorias++;
        }
        $data_victorias[] = [
            'name' => 'Etapa ' . $etapa,
            'y' => $total_victorias,
        ];
    }

    $etapas_names = array_map(function($etapa) {
        return 'Etapa ' . $etapa;
    }, $etapas);

    $chart_victorias = [
        'chart' => ['type' => 'line'],
        'title' => ['text' => 'Victorias del equipo'],
        'xAxis' => ['categories' => array_slice($etapas_names, 0),],
        'yAxis' => [
            'title' => ['text' => 'Número de victorias'],
            'min' => 0,
            'max' => $max_victorias,
        ],
        'series' => [['name' => 'Victorias', 'data' => $data_victorias]],
    ];
    
    $data_mge = [];
    $total_mge = 0;
    
    foreach ($etapas as $etapa) {
        if (in_array($etapa, $mge)) {
            $total_mge++;
        }
        $data_mge[] = [
            'name' => 'Etapa ' . $etapa,
            'y' => $total_mge,
        ];
    }
    
    $data_mmo = [];
    $total_mmo = 0;
    
    foreach ($etapas as $etapa) {
        if (in_array($etapa, $mmo)) {
            $total_mmo++;
        }
        $data_mmo[] = [
            'name' => 'Etapa ' . $etapa,
            'y' => $total_mmo,
        ];
    }
    
    $data_mmv = [];
    $total_mmv = 0;
    
    foreach ($etapas as $etapa) {
        if (in_array($etapa, $mmv)) {
            $total_mmv++;
        }
        $data_mmv[] = [
            'name' => 'Etapa ' . $etapa,
            'y' => $total_mmv,
        ];
    }
    
    $data_mre = [];
    $total_mre = 0;
    
    foreach ($etapas as $etapa) {
        if (in_array($etapa, $mre)) {
            $total_mre++;
        }
        $data_mre[] = [
            'name' => 'Etapa ' . $etapa,
            'y' => $total_mre,
        ];
    }
    
    $data_mse = [];
    $total_mse = 0;
    
    foreach ($etapas as $etapa) {
        if (in_array($etapa, $mse)) {
            $total_mse++;
        }
        $data_mse[] = [
            'name' => 'Etapa ' . $etapa,
            'y' => $total_mse,
        ];
    }
    
    $data_mms = [];
    $total_mms = 0;
    
    foreach ($etapas as $etapa) {
        if (in_array($etapa, $mms)) {
            $total_mms++;
        }
        $data_mms[] = [
            'name' => 'Etapa ' . $etapa,
            'y' => $total_mms,
        ];
    }
    
    $chart_maillots = [
        'chart' => ['type' => 'line'],
        'title' => ['text' => 'Maillots obtenidos'],
        'xAxis' => ['categories' => array_slice($etapas_names, 0),],
        'yAxis' => [
            'title' => ['text' => 'Número de maillots'],
            'min' => 0,
            'max' => $max_maillots,
        ],
        'colors' => ['#FFFF00', '#ff9E81', '#800080', '#FF0000', '#008000', '#FFC0C0'],
        'series' => [['name' => 'MGE', 'data' => $data_mge], ['name' => 'MMO', 'data' => $data_mmo], ['name' => 'MMV', 'data' => $data_mmv], ['name' => 'MRE', 'data' => $data_mre], ['name' => 'MSE', 'data' => $data_mse], ['name' => 'MMS', 'data' => $data_mms]]
    ];
    
?>


<div class="site-index">
    
    <div class="jumbotron text-center" style="background-image: url('https://images.pexels.com/photos/2876511/pexels-photo-2876511.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1'); background-size: cover; background-position: center; color: #FFBD59;">
        <h1 class="display-4"><?= strtoupper($equipo) ?></h1>
    </div>

    <div class="container" style="margin-top: -64px !important;">
        
        <div class="row justify-content-center">
            <div class="col-md-7">
                <p class="text-justify"><?= $texto[$equipo] ?></p>
            </div>
            <div class="col-md-3">
                <?= Html::img('@web/images/banner_' . $equipo . '.jpg', ['alt' => 'Banner ' . ucwords($equipo), 'class' => 'img-fluid']) ?>
            </div>
        </div>
        
        <hr class="my-4">
        
        <div class="row justify-content-center">
            <div class="col-md-3">
                <h4 class="text-center">COMPONENTES DEL EQUIPO</h4>
                <p style="font-weight: bold"><?= ucwords($director) ?> (D)</p>
                <?php foreach ($ciclistas as $ciclista): ?>
                    <p style="font-weight: bold"><?= ucwords($ciclista) ?></p>
                <?php endforeach; ?>
            </div>
            <div class="col-md-7">
                <?= HighCharts::widget(['clientOptions' => $chart_victorias]); ?>
            </div>
        </div>
        
        <hr class="my-4">
        
        <div class="row justify-content-center">
            <div class="col-md-10">
                <?= HighCharts::widget(['clientOptions' => $chart_maillots]); ?>
            </div>
        </div>
        
    </div>

</div>