
<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\bootstrap\BootstrapAsset;
use yii\widgets\LinkPager;

// Definición del título
$this->title = 'Los equipos';

?>


<div class="body-content mt-5">
       <style>
            .body-content {
          margin-left: 15px;
      }

      /* Ajuste para evitar que el margen afecte al header y al footer */
      header, footer {
          margin-left: 0px;
          width: calc(100% + 80px);
      }
    </style>
    <div class="row">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_equipos', // Vista parcial para cada tarjeta
            'layout' => "{items}",
            'options' => ['class' => 'row'], // Agregado para envolver las tarjetas en una fila
            'itemOptions' => ['class' => 'col-sm-4 col-md-3 mb-3'], // Ajustado para ocupar 4 unidades en pantallas pequeñas
        ]); ?>
    </div>
</div>

<div class="row">
        <div class="col-sm-12 text-center"> <!-- Centra la paginación -->
            <?= LinkPager::widget([
                'pagination' => $dataProvider->pagination,
            ]); ?>
        </div>
    </div>
