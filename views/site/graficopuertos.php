<?php
//Highcharts de JavaScript
use yii\helpers\Html;
use yii\helpers\Json;
use dosamigos\highcharts\HighCharts;

$data = [];
foreach ($resultados as $resultado) {
    $data[] = [
        'name' => $resultado['nompuerto'],
        'y' => (float) $resultado['altura_puerto'], // Cambiado a float para aceptar valores decimales
    ];
}



$chartConfig = [
    'chart' => ['type' => 'line'],
    'title' => ['text' => 'Alturas de los puertos'],
    'xAxis' => ['categories' => array_column($resultados, 'nompuerto')],
    'yAxis' => ['title' => ['text' => 'Altura del puerto']],
    'series' => [['name' => 'Altura del puerto', 'data' => $data]],
];

echo HighCharts::widget(['clientOptions' => $chartConfig]);

echo Html::tag('div', '', ['id' => 'grafico-puerto']);

//Texto e imagenes después del gráfico
echo '<div>';
echo '<div class="d-flex align-items-center">';
echo '<div style="margin-left: 200px;">'; // Margen a la izquierda de la imagen
echo Html::img('@web/images/naranco_2.jpg', ['alt' => Yii::$app->name, 'style' => 'padding-top: 20px;']); // Añadir padding-top a la imagen
echo '</div>';
echo '<div style="margin-top: 50px;">'; // Margen superior del texto
echo '<h2 style="padding-right: 25%; padding-left: 10%;">El Alto del Naranco</h2>';
echo '<p style="padding-right: 25%; padding-left: 10%;">El Alto del Naranco es un emblemático puerto de montaña ubicado en la región de Asturias, España. Con una altitud de aproximadamente 634 metros sobre el nivel del mar, este puerto desafía a los ciclistas con sus empinadas pendientes y sus impresionantes vistas panorámicas.</p>';
echo '</div>';
echo '</div>';
echo '</div>';

echo '<div>';
echo '<div class="d-flex align-items-center">';
echo '<div style="margin-left: 200px;">'; // Margen a la izquierda de la imagen
echo Html::img('@web/images/sierra2.jpg', ['alt' => Yii::$app->name, 'style' => 'padding-top: 20px;']); // Añadir padding-top a la imagen
echo '</div>';
echo '<div style="margin-top: 50px;">'; // Margen superior del texto
echo '<h2 style="padding-right: 25%; padding-left: 10%;">Sierra Nevada</h2>';
echo '<p style="padding-right: 25%; padding-left: 10%;">Sierra Nevada, un icónico destino ciclista, se alza majestuosa en el sur de España, en la provincia de Granada. Este impresionante macizo montañoso es conocido tanto por su belleza natural como por su desafiante terreno que atrae a ciclistas de todo el mundo.</p>';
echo '</div>';
echo '</div>';
echo '</div>';


// Añadimos más secciones similares si es necesario


$this->registerJsFile('@web/js/highcharts.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJsFile('@web/js/exporting.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJsFile('@web/js/export-data.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerCssFile('@web/css/highcharts.css');