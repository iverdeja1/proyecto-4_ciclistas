<?php
use yii\grid\GridView;
?>

<style>
    .jumbotron {
        background-color: #092b5f; /* Fondo azul */
        color: #FFBD59; /* Texto blanco */
        text-align: center; /* Texto centrado */
        padding: 20px; /* Espaciado interno */
        margin-bottom: 20px; /* Espaciado inferior */
    }
    .jumbotron h2, .jumbotron .lead {
        color: #FFBD59; /* Texto blanco */
    }
    .table-container {
        margin-left: 20px; /* Ajusta el margen izquierdo según tus necesidades */
        margin-right: 20px; /* Ajusta el margen derecho según tus necesidades */
    }
</style>

<div class="jumbotron">
    <h2><?= $titulo ?></h2>
    <p class="lead"><?= $enunciado ?></p>
</div>

<div class="table-container">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $campos,
        'tableOptions' => ['class' => 'table'], // Agregar clase de Bootstrap para estilos de tabla
    ]); ?>
</div>