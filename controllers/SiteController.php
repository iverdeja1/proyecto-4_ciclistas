<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Equipo;
use app\models\Puerto;
use app\models\Lleva;
use app\models\Etapa;
use yii\data\SqlDataProvider;
use yii\db\Query;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    
    
    /*Este método se usa para mostrar el ListView*/
    public function actionMostrarciclistas(){
        $dataProvider = new ActiveDataProvider ([
            'query' => Ciclista::find(),
        ]);
        return $this->render("ciclistas",[
            "dataProvider"=>$dataProvider,
        ]);
    }
     
    
    public function actionMostrarequipos(){
        $dataProvider = new ActiveDataProvider([
        'query' => Equipo::find(),
        'pagination' => [
            'pageSize' => 12, // Ajusta según sea necesario
        ],
    ]);

    return $this->render('equiposs', [
        'dataProvider' => $dataProvider,
    ]);
    }
    
    /* 
    public function actionEtapas(){
          // Mediante DAO
       $dataProvider = new \yii\data\SqlDataProvider([
           'sql' => 'select distinct dorsal from etapa group by dorsal having count(*)>1',
       ]);

       return $this->render("resultado", [
           "resultados" => $dataProvider,
           "campos" => ['dorsal'],
           "titulo" => "Consulta 12 con DAO",
           "enunciado" => "Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
           "sql" => " select distinct dorsal from etapa group by dorsal having count(*)>1;",
       ]);
        
    }
    */
   public function actionEtapas()
    {
        $sql = 'SELECT e.*, c.nombre AS CiclistaLider
                FROM etapa e
                LEFT JOIN lleva l ON e.numetapa = l.numetapa
                LEFT JOIN ciclista c ON l.dorsal = c.dorsal
                WHERE l.código = :codigo';

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'params' => [':codigo' => 'MGE'],
            'pagination' => [
                'pageSize' => 10, // Ajusta según tus necesidades
            ],
        ]);

        return $this->render('resultado2', [
            'resultados' => $dataProvider,
            'campos' => $dataProvider->getModels() ? array_keys($dataProvider->getModels()[0]) : [],
            'titulo' => 'Consulta de ciclistas',
            'enunciado' => 'Etapas donde el ciclista líder tiene el código MGE',
//            'sql' => $sql,
        ]);
    }
    
   

            public function actionPuertos()
          {
              $dataProvider = new \yii\data\SqlDataProvider([
                  'sql' => 'SELECT nompuerto, altura, pendiente FROM puerto WHERE pendiente > 5.0',
              ]);

              return $this->render('resultado', [
                  'dataProvider' => $dataProvider, // Corregido el nombre de la variable
                  'campos' => ['nompuerto', 'altura', 'pendiente'], // Lista de campos para mostrar en la vista
                  'titulo' => "Consulta de puertos",
                  'enunciado' => "Puertos con pendiente superior al 5%",
              ]);
          }
          
          

            public function actionEquipos()
          {
              $dataProvider = new \yii\data\SqlDataProvider([
                'sql' => 'SELECT COUNT(*) AS total_ciclistas, nomequipo FROM ciclista GROUP BY nomequipo',
            ]);

            return $this->render('resultado', [
                'dataProvider' => $dataProvider,
                'campos' => ['total_ciclistas', 'nomequipo'], // Lista de campos para mostrar en la vista
                'titulo' => "Consulta de Equipos",
                'enunciado' => "Número de Ciclistas por Equipo",
            ]);
          }


        public function actionAlturapuertos()
    {
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT nompuerto, altura as altura_puerto FROM puerto GROUP BY nompuerto',
        ]);

        $resultados = $dataProvider->getModels();

        return $this->render('graficopuertos', [
            'dataProvider' => $dataProvider,
            'resultados' => $resultados,
        ]);
    }
    
       public function actionCiclistamge()
    {
        $dataProvider = new SqlDataProvider([
            'sql' => 'select nombre, COUNT(*) AS victorias from lleva inner join ciclista using(dorsal) where código="MGE" group BY dorsal',
        ]);

        $resultados = $dataProvider->getModels();

        return $this->render('graficomge(amarillo)', [
            'dataProvider' => $dataProvider,
            'resultados' => $resultados,
        ]);
    }
    
public function actionMostrarequipo($equipo) {
    
    $director = Equipo::find()->select("director")->distinct()->where(["nomequipo" => $equipo])->scalar();
    
    $ciclistas = Ciclista::find()->select("nombre")->distinct()->where(["nomequipo" => $equipo])->column();
    
    $etapas = Etapa::find()->select("numetapa")->distinct()->orderBy(['numetapa' => SORT_ASC])->column();
    
    $victorias = Etapa::find()->select('etapa.numetapa')->distinct()->innerJoin('ciclista', 'etapa.dorsal = ciclista.dorsal')->where(['ciclista.nomequipo' => $equipo])->column();
    $max_victorias = Etapa::find()->select('MAX(c1.victorias)')->from(['c1' => (new \yii\db\Query())->select('COUNT(*) AS victorias')->from('etapa')->innerJoin('ciclista', 'etapa.dorsal = ciclista.dorsal')->groupBy('ciclista.nomequipo')])->scalar();
    
    $mge = Lleva::find()->select('lleva.numetapa')->distinct()->innerJoin('ciclista', 'lleva.dorsal = ciclista.dorsal')->where(['ciclista.nomequipo' => $equipo, 'lleva.código' => 'mge'])->distinct()->column();
    $mmo = Lleva::find()->select('lleva.numetapa')->distinct()->innerJoin('ciclista', 'lleva.dorsal = ciclista.dorsal')->where(['ciclista.nomequipo' => $equipo, 'lleva.código' => 'mmo'])->distinct()->column();
    $mmv = Lleva::find()->select('lleva.numetapa')->distinct()->innerJoin('ciclista', 'lleva.dorsal = ciclista.dorsal')->where(['ciclista.nomequipo' => $equipo, 'lleva.código' => 'mmv'])->distinct()->column();
    $mre = Lleva::find()->select('lleva.numetapa')->distinct()->innerJoin('ciclista', 'lleva.dorsal = ciclista.dorsal')->where(['ciclista.nomequipo' => $equipo, 'lleva.código' => 'mre'])->distinct()->column();
    $mse = Lleva::find()->select('lleva.numetapa')->distinct()->innerJoin('ciclista', 'lleva.dorsal = ciclista.dorsal')->where(['ciclista.nomequipo' => $equipo, 'lleva.código' => 'mse'])->distinct()->column();
    $mms = Lleva::find()->select('lleva.numetapa')->distinct()->innerJoin('ciclista', 'lleva.dorsal = ciclista.dorsal')->where(['ciclista.nomequipo' => $equipo, 'lleva.código' => 'mms'])->distinct()->column();
    $max_maillots = Lleva::find()->select('MAX(c1.maillots)')->from(['c1' => (new \yii\db\Query())->select('COUNT(*) AS maillots')->from('lleva')->innerJoin('ciclista', 'lleva.dorsal = ciclista.dorsal')->groupBy(['ciclista.nomequipo', 'lleva.código'])])->scalar();
    
    return $this->render('equipo', [
        'equipo' => $equipo,
        'texto' => [
            'amore vita' => 'El Amore & Vita-Prodir (código UCI: AMO) es un equipo ciclista profesional de categoría Continental con licencia ucraniana, aunque su estructura es italiana. Fue fundado en 1989 por Ivano Fanini. A lo largo de su historia, el equipo ha registrado alrededor de 3000 victorias en todas las categorías, incluyendo 12 Campeonatos del Mundo y 50 títulos nacionales (entre Italia y otros países). Ciclistas destacados como Franco Chioccioli, Pierino Gavazzi, Gianbattista Baronchelli, Mario Cipollini, Michele Bartoli, Andrea Tafi y Rolf Sørensen fueron descubiertos por Ivano Fanini. El nombre "Amore & Vita" (Amor y Vida) fue creado por Ivano Fanini, quien estuvo ligado a la Santa Sede y al Papa Juan Pablo II. Durante la presentación y bendición del equipo en la Ciudad del Vaticano por el propio Papa en 1989, decidió hacer conocer su pensamiento respecto al aborto. Para llevar su testimonio a favor de la vida, hizo escribir en los maillots de sus corredores el lema "NO AL ABORTO". El equipo ha sido registrado en diversos países a lo largo de su historia.',
            'artiach' => 'El Artiach fue un equipo ciclista profesional español que existió desde 1984 hasta 1995. Su patrocinador principal era el fabricante de galletas bilbaíno Artiach. Antes de convertirse en Artiach, el equipo era conocido como Orbea, Seat, Caja Rural y Paternina. Durante su existencia, el equipo tuvo varias figuras destacadas, aunque también sufrió varias bajas importantes. A pesar de los cambios en la plantilla, el equipo logró algunas victorias notables, incluyendo etapas en la Vuelta a Murcia, la Vuelta a Valencia y la Vuelta a España. En 1996, el equipo se fusionó con el Kelme-Sureña, convirtiéndose en Kelme-Artiach.',
            'banesto' => 'El Banesto fue un equipo ciclista profesional español que existió desde 1980 hasta 2003. Inicialmente se llamaba Reynolds, patrocinado por la empresa de aluminio, y estaba dirigido por José Miguel Echavarri y Eusebio Unzué. En 1989, cambió su nombre y pasó a ser patrocinado por el banco español Banesto. El equipo es conocido por sus logros significativos en el ciclismo, especialmente durante la era de Miguel Induráin, quien ganó cinco Tours de Francia consecutivos entre 1991 y 1995, y dos Giros de Italia en 1992 y 1993. En 1992 y 1993, el equipo fue clasificado como el mejor equipo en el ranking UCI. Después de la retirada de Induráin en 1996, Abraham Olano y el "Chava" Jiménez se convirtieron en las principales figuras del equipo, logrando como mayor éxito la Vuelta de 1998, en la que fueron 1º y 3º respectivamente. En 2004, el equipo encontró un nuevo patrocinador en el Gobierno de las Islas Baleares, cambiando su nombre a Illes Balears-Banesto. En 2005, Banesto se retiró definitivamente del patrocinio del equipo. El equipo es considerado el predecesor del actual Movistar Team.',
            'bresciali-refin' => 'El equipo de ciclismo Brescialat, también conocido como Tenax, Liquigas-Pata o Cage Maglierie, fue un equipo ciclista irlandés, de origen italiano, que compitió en ciclismo en ruta desde 1994 hasta 2007. El equipo se fundó en 1994 con el nombre de Brescialat-Ceramiche Refin, y ya en su primer año participó en el Giro de Italia y la Vuelta a España. En 1995, el director deportivo Primo Franchini dejó el equipo, junto con el segundo patrocinador, para establecer el Refin-Cantina Tollo. A partir de 1999, Brescialat dejó el patrocinio y fue sustituido por Liquigas durante tres años. En 2002, Cage Maglierie se convirtió en el principal patrocinador, hasta que en 2003 pasó a ser de Tenax. Con la entrada de la UCI World Tour, el equipo pasó a ser Continental Profesional, pudiendo participar en los circuitos continentales. Al mismo tiempo, el equipo pasó a tener licencia irlandesa. El equipo desapareció a finales de la temporada 2007. Durante su existencia, el equipo logró varias victorias en el Giro de Italia y la Vuelta a España. Algunos de sus corredores más destacados fueron Flavio Giupponi, Paolo Lanfranchi, Zenon Jascuła, Wladimir Belli, Roberto Sgambelluri, Nicola Miceli, Serhi Honchar, entre otros.',
            'carrera' => 'El equipo de ciclismo Carrera fue un equipo italiano que compitió en ciclismo en ruta desde 1979 hasta 1996. Su principal patrocinador era el fabricante de ropa vaquera italiano Carrera Jeans. El equipo se fundó como Inoxpran en 1979, con Giovanni Battaglin como principal líder de equipo y Davide Boifava como director deportivo. En 1981, Battaglin ganó la clasificación general de la Vuelta a España y del Giro de Italia, convirtiéndose en el segundo corredor de la historia en ganar el doblete Giro-Vuelta. En 1984, Carrera Jeans se convirtió en el principal patrocinador. Durante su existencia, el equipo logró importantes victorias y fue referencia del pelotón internacional durante varios años, consiguiendo ganar la clasificación general de Giro de Italia y Tour de Francia. Algunos de los ciclistas más destacados del equipo incluyen a Giovanni Battaglin, Guido Bontempi, Urs Zimmermann, Roberto Visentini, entre otros.',
            'castorama' => 'El equipo de ciclismo Castorama, de origen francés, compitió en el circuito profesional entre 1990 y 1995. Este equipo es recordado como el sucesor del antiguo equipo Système U. Durante su existencia, Castorama logró destacarse en varias competencias. Entre sus logros más notables se encuentran la victoria de Laurent Fignon en el Critérium Internacional en 1990, la victoria de Jacky Durand en el Tour de Flandes en 1992, y las victorias de Thomas Davy y Emmanuel Magnien en el Tour del Porvenir en 1993 y 1995 respectivamente. Además, Armand de Las Cuevas ganó la Clásica de San Sebastián en 1994. En las grandes vueltas, el equipo participó en el Tour de Francia y el Giro de Italia en seis ocasiones, logrando un total de 10 victorias de etapa. El equipo Castorama desapareció al final de la temporada 1995.',
            'euskadi' => 'El equipo de ciclismo Euskadi, también conocido como Euskaltel-Euskadi, fue un equipo ciclista profesional español que compitió desde 1994 hasta 2012. Este equipo se caracterizó por tener corredores originarios del País Vasco, Navarra o País Vasco francés. Fue fundado en 1994 por la Fundación Euskadi, una organización sin ánimo de lucro. En 1997, la compañía telefónica del País Vasco Euskaltel se convirtió en el patrocinador principal del equipo. Durante su existencia, el equipo logró varias victorias en las grandes vueltas, incluyendo el Tour de Francia y la Vuelta a España. En 2013, debido a cambios en la UCI y la escasez de resultados, el equipo cambió su filosofía y comenzó a contratar ciclistas de otras regiones de España y extranjeros. Sin embargo, este cambio no fue suficiente para mantener al equipo, que desapareció al final de la temporada 2012. En 2019, el equipo fue revivido bajo el nombre de Fundación-Orbea y compitió en la segunda división mundial. En 2020, el equipo recuperó su nombre original, Euskaltel-Euskadi, y continúa compitiendo con la esperanza de volver a su antigua grandeza.',
            'gatorade' => 'El equipo de ciclismo Gatorade fue un equipo ciclista profesional italiano dirigido por Giuanluigi Stanga. El equipo surgió para la temporada 1983. En 1994, Polti llegó como nuevo patrocinador principal. La última temporada del equipo fue la de 2000. Durante su existencia, el equipo pasó por varias denominaciones, incluyendo Gatorade-Chateau d\'Ax y Gatorade-Bianchi. Algunos de los corredores más destacados del equipo incluyen a Laurent Fignon, Djamolidine Abdoujaparov, Gianni Bugno, Ivan Gotti, y Richard Virenque. El equipo logró varias victorias importantes en carreras clásicas y por etapas, incluyendo el Tour de Flandes, la Amstel Gold Race, y la Lieja-Bastoña-Lieja.',
            'gewiss' => 'El equipo de ciclismo Gewiss fue un equipo ciclista profesional italiano de mediados de la década de 1990. El equipo se creó para la temporada 1993. Sus dos patrocinadores principales eran las compañías italianas Mecair y Ballan. El proyecto estaría liderado por el director deportivo Emmanuelle Bombini. Durante su existencia, el equipo pasó por varias denominaciones, incluyendo Gewiss-Ballan y Gewiss-Playbus. Algunos de los corredores más destacados del equipo incluyen a Eugeni Berzin, Ivan Gotti, Bjarne Riis, Piotr Ugrumov, Giorgio Furlan, Nicola Minali, Moreno Argentin y Stefano Zanini. El equipo logró varias victorias importantes en carreras clásicas y por etapas, incluyendo la Tirreno-Adriático, la Milán-San Remo, la Flecha Valona, la Lieja Bastoña-Lieja y el Giro de Italia. Piotr Ugrumov fue segundo en el Tour de Francia.',
            'jolly club' => 'El Jolly Club fue un equipo de ciclismo italiano que compitió en varios campeonatos de rally, incluyendo el Campeonato del Mundo de Rally, el Campeonato de Europa y el Campeonato de Italia. También participaron en la Fórmula 1, principalmente con vehículos de las marcas Lancia y Alfa Romeo. En 1989, el equipo se llamó Jolly - Club 88 y utilizó bicicletas Gios. El gerente del equipo fue Enrico Paolini. Aunque su enfoque principal estaba en el rally, su presencia en el mundo del ciclismo también es notable. Si bien no tengo detalles específicos sobre su desempeño en el ciclismo, su legado en el automovilismo es significativo.',
            'kelme' => 'El Kelme fue un equipo ciclista español fundado en 1980 como heredero del desaparecido Transmallorca-Flavia-Gios. Estuvo patrocinado por la empresa deportiva Kelme hasta que en 2004, la Generalidad Valenciana asumió la deuda del mismo y se hizo cargo del equipo hasta su desaparición al final de 2006. A lo largo de su historia, el Kelme compitió en la categoría UCI WorldTeam y tuvo una relación significativa con Colombia, incorporando a muchos ciclistas de dicha nacionalidad. En dos ocasiones (en 1989 y 1992), su licencia se registró como equipo colombiano. El director del equipo en su última época fue el excorredor Vicente Belda, y el director adjunto fue José Ignacio Labarta. Por las filas de este equipo pasaron corredores destacados como Laudelino Cubino, Álvaro Pino, Fabio Parra, Fernando Escartín, Roberto Heras, Aitor González, Alejandro Valverde, Enrique Martínez, José Recio y Óscar Sevilla. Anteriormente, el equipo fue dirigido por Rafa Carrasco y Álvaro Pino. Sin embargo, en 2006, el equipo se disolvió tras verse implicados varios de sus altos cargos en la llamada Operación Puerto. A pesar de los desafíos, el Kelme dejó un legado en el mundo del ciclismo y su plantilla y estructura directiva fueron asumidas en 2007 por el nuevo equipo Fuerteventura-Canarias, considerado su sucesor en el pelotón internacional.',
            'lotus festina' => 'El Festina fue un equipo ciclista profesional de carretera patrocinado por el fabricante de relojes Festina. Inicialmente, se llamó Lotus-Zahor y heredó la estructura del equipo Hueso. En 1990, el equipo adoptó la denominación Lotus-Festina, manteniendo esta doble identidad durante un tiempo: Lotus para las carreras en España y Festina para el resto del mundo. A lo largo de su historia, el Festina tuvo una relación con Colombia, incorporando a muchos ciclistas de dicha nacionalidad. Durante los años siguientes, el equipo se consolidó como uno de los más destacados en el pelotón internacional. En 1994, el equipo se convirtió verdaderamente en francés, y su director deportivo, Bruno Roussel, reforzó la plantilla con corredores como Luc Leblanc, quien ganó el Campeonato del Mundo ese mismo año. Richard Virenque, Pascal Lino y Thierry Marie también formaron parte del equipo, y Virenque obtuvo el título de mejor escalador del Tour de Francia en cuatro ocasiones consecutivas entre 1994 y 1997. En 1997, Laurent Brochard logró el Campeonato del Mundo. En 1998, el Festina fue reconocido como el más fuerte del mundo con la incorporación de la estrella suiza Alex Zülle.',
            'mapei-clas' => 'El equipo de ciclismo Mapei fue un equipo italiano de renombre internacional que compitió en el pelotón profesional desde principios de la década de 1990 hasta principios de la década de 2000. Fue patrocinado por la empresa de productos químicos y de construcción Mapei. El equipo Mapei era conocido por su dominio en las clásicas de primavera, como la París-Roubaix y la Giro de Lombardía, así como en carreras por etapas importantes como la París-Niza y la Tirreno-Adriático. Uno de los sellos distintivos del equipo Mapei fue su habilidad para competir en terrenos adoquinados, como los tramos famosos de la París-Roubaix. Esto se debió en parte al desarrollo de tecnologías de bicicletas y neumáticos específicos para este tipo de terreno. El equipo contó con algunos de los mejores ciclistas de la época, incluyendo a figuras como Johan Museeuw, Franco Ballerini, Andrea Tafi, Paolo Bettini, y Michele Bartoli, entre otros. El equipo Mapei ganó numerosas carreras importantes y títulos mundiales, tanto en carretera como en contrarreloj individual. También dominaron el calendario clásico y estuvieron en la cima del ranking de la Unión Ciclista Internacional (UCI) durante varios años. A pesar de su éxito, el equipo Mapei se disolvió a principios de la década de 2000 debido a cambios en el patrocinio y en la estructura del ciclismo profesional. Muchos de sus corredores pasaron a competir en otros equipos después de su desaparición. En resumen, el equipo de ciclismo Mapei dejó un legado duradero en el mundo del ciclismo profesional, gracias a su dominio en una amplia gama de carreras y su enfoque en la innovación tecnológica y táctica.',
            'mercatone uno' => 'El equipo de ciclismo Mercatone Uno fue un equipo italiano que compitió en el pelotón profesional durante las décadas de 1990 y principios de 2000. Este equipo estaba patrocinado principalmente por la cadena de tiendas de muebles y electrodomésticos Mercatone Uno. Mercatone Uno fue fundado en 1992 y obtuvo su patrocinio principal de la cadena de tiendas homónima. El patrocinador principal, Mercatone Uno, era una empresa italiana conocida por su enfoque en artículos para el hogar. El equipo Mercatone Uno se destacó principalmente en el Giro de Italia, la gran vuelta por etapas de Italia. Durante su existencia, el equipo logró victorias y destacadas actuaciones en esta competición. Uno de los ciclistas más destacados asociados con el equipo Mercatone Uno fue Marco Pantani, un talentoso escalador italiano que ganó el Giro de Italia y el Tour de Francia en 1998. Pantani fue una figura emblemática del equipo y alcanzó un gran éxito en las montañas, convirtiéndose en uno de los ciclistas más queridos y recordados en la historia del ciclismo italiano. Además de Pantani, el equipo Mercatone Uno contó con otros ciclistas notables a lo largo de los años, aunque ninguno alcanzó el mismo nivel de fama y éxito que el Pirata. Algunos de estos ciclistas incluyeron a Gilberto Simoni, Fabio Baldato, y Davide Rebellin, entre otros. Después de la retirada de Pantani en 2004 y los problemas financieros, el equipo Mercatone Uno se disolvió. Aunque el equipo tuvo momentos de éxito y dejó una marca significativa en el ciclismo italiano, su legado se vio eclipsado en gran medida por la trágica historia personal de Marco Pantani y los desafíos financieros del equipo.',
            'motorola' => 'El equipo de ciclismo Motorola fue respaldado por la empresa de telecomunicaciones estadounidense Motorola durante los años 90. Este equipo destacó en el pelotón internacional, especialmente en el Tour de Francia. Uno de los ciclistas más notables asociados con el equipo fue Lance Armstrong, quien comenzó su carrera profesional con ellos. Aunque el equipo se disolvió a finales de la década de 1990, dejó un legado significativo en el ciclismo profesional, sobre todo por el papel que desempeñó en el surgimiento de Armstrong como una figura prominente en el deporte, a pesar de las controversias posteriores.',
            'navigare' => 'El equipo de ciclismo Navigare fue un equipo italiano que compitió en el pelotón profesional durante los años 2000. Este equipo estaba patrocinado por Navigare, una empresa italiana de moda. Aunque no alcanzó los niveles de éxito de otros equipos más grandes, como el Giro de Italia, participó en diversas competiciones ciclistas, principalmente en el circuito italiano. A lo largo de su existencia, el equipo contó con varios ciclistas italianos y extranjeros, aunque no logró destacarse de manera significativa en el panorama ciclista internacional. A pesar de su desaparición relativamente discreta, el equipo dejó su marca en el ciclismo italiano y contribuyó al desarrollo del deporte en el país.',
            'once' => 'El equipo de ciclismo ONCE fue un equipo español que compitió en el pelotón profesional durante varias décadas, desde los años 80 hasta principios de los 2000. Respaldado por la Organización Nacional de Ciegos Españoles (ONCE), el equipo no solo destacó en el ámbito deportivo, sino que también sirvió como una plataforma de inclusión social para personas con discapacidad visual. Con un enfoque en el ciclismo de carretera, el equipo ONCE cosechó numerosos éxitos en grandes vueltas como el Tour de Francia, la Vuelta a España y el Giro de Italia, así como en otras competiciones de renombre mundial. Además de su éxito deportivo, el equipo se destacó por su compromiso con la inclusión y la visibilidad de las personas con discapacidad visual, promoviendo la igualdad y la conciencia social. Aunque el equipo desapareció a principios de los 2000 debido a cambios en el patrocinio y la estructura del ciclismo profesional, su legado perdura como un ejemplo de cómo el deporte puede ser una herramienta poderosa para la inclusión y la diversidad.',
            'pdm' => 'El equipo de ciclismo PDM fue un equipo holandés que compitió en el pelotón profesional durante la década de 1980 y principios de los 90. Patrocinado por la empresa farmacéutica holandesa PDM, el equipo logró destacados éxitos en carreras por etapas y clásicas del ciclismo mundial. Con ciclistas como Erik Breukink, Sean Kelly y Gert-Jan Theunisse, el equipo se destacó en el Tour de Francia, la Vuelta a España y la París-Niza, entre otras competiciones. A pesar de sus éxitos en la carretera, el equipo PDM se vio afectado por escándalos de dopaje, lo que finalmente condujo a su disolución a mediados de la década de 1990. Aunque su existencia fue relativamente corta, el equipo PDM dejó una marca significativa en el ciclismo profesional y sigue siendo recordado por su participación en algunas de las carreras más importantes del mundo.',
            'seguros amaya' => 'El equipo de ciclismo Seguros Amaya fue un equipo español que compitió en el pelotón profesional durante la década de 1980 y principios de los 90. Patrocinado por la empresa de seguros Seguros Amaya, el equipo participó en varias competiciones ciclistas importantes, principalmente en España y otros países europeos. Aunque no alcanzó los mismos niveles de éxito que algunos de sus competidores más grandes, el equipo Seguros Amaya proporcionó una plataforma para ciclistas españoles emergentes y contribuyó al desarrollo del ciclismo en el país. A pesar de su desaparición a principios de los 90, su legado sigue siendo recordado en la historia del ciclismo español.',
            'telecom' => 'El equipo de ciclismo Telecom fue un destacado equipo italiano que compitió en el pelotón profesional durante los años 90 y principios de los 2000. Respaldado por la empresa de telecomunicaciones Telecom Italia, el equipo destacó en varias competiciones, incluyendo el Giro de Italia y otras carreras por etapas importantes. Contó con ciclistas de renombre como Mario Cipollini, quien fue conocido como el Rey de la Velocidad y ganó múltiples etapas en el Giro de Italia y el Tour de Francia. Aunque el equipo tuvo éxito en el circuito italiano y algunos eventos internacionales, se disolvió a principios de los 2000 debido a cambios en el patrocinio y la estructura del ciclismo profesional. A pesar de su desaparición, el equipo Telecom dejó un legado duradero en el ciclismo italiano y sigue siendo recordado por su dominio en las carreras de velocidad y su contribución al desarrollo del deporte en Italia.',
            'tvm' => 'El equipo de ciclismo TVM fue un equipo holandés que compitió en el pelotón profesional durante las décadas de 1980 y 1990. Patrocinado por la empresa de transportes TVM, el equipo destacó en diversas competiciones ciclistas, incluyendo el Tour de Francia, la Vuelta a España y clásicas importantes como la París-Roubaix. A lo largo de su existencia, el equipo contó con ciclistas de renombre como Phil Anderson, Jean-Paul van Poppel y Gert-Jan Theunisse, entre otros. Sin embargo, el equipo también enfrentó controversias y escándalos de dopaje a lo largo de los años, lo que afectó su reputación. A pesar de su desaparición a finales de la década de 1990 debido a problemas financieros, el equipo TVM dejó un legado en el ciclismo holandés y europeo, siendo recordado por su participación en algunas de las carreras más emblemáticas del mundo y por su contribución al desarrollo del deporte en los Países Bajos.',
            'wordperfect' => 'El equipo de ciclismo WordPerfect fue un equipo neerlandés que compitió en el pelotón profesional durante la década de 1990. Patrocinado por la empresa de software WordPerfect Corporation, el equipo participó en varias competiciones ciclistas importantes, destacando en carreras de un día y etapas. Aunque no alcanzó los mismos niveles de éxito que algunos de sus competidores más grandes, el equipo WordPerfect proporcionó una plataforma para ciclistas neerlandeses emergentes y contribuyó al desarrollo del ciclismo en los Países Bajos. A pesar de su corta existencia, su legado sigue siendo recordado en la historia del ciclismo neerlandés.',
        ],
        'director' => $director,
        'ciclistas' => $ciclistas,
        'etapas' => $etapas,
        'victorias' => $victorias,
        'max_victorias' => $max_victorias,
        'mge' => $mge,
        'mmo' => $mmo,
        'mmv' => $mmv,
        'mre' => $mre,
        'mse' => $mse,
        'mms' => $mms,
        'max_maillots' => $max_maillots,
    ]);
    
}

public function actionMostrarmaillots(){        
        
        $mgeData = Lleva::find()->select(['nombre', 'COUNT(*) AS llevado'])->innerJoin('ciclista', 'lleva.dorsal = ciclista.dorsal')->where(['código' => 'MGE'])->groupBy('lleva.dorsal')->asArray()->all();
        $mmoData = Lleva::find()->select(['nombre', 'COUNT(*) AS llevado'])->innerJoin('ciclista', 'lleva.dorsal = ciclista.dorsal')->where(['código' => 'MMO'])->groupBy('lleva.dorsal')->asArray()->all();
        $mmvData = Lleva::find()->select(['nombre', 'COUNT(*) AS llevado'])->innerJoin('ciclista', 'lleva.dorsal = ciclista.dorsal')->where(['código' => 'MMV'])->groupBy('lleva.dorsal')->asArray()->all();
        $mreData = Lleva::find()->select(['nombre', 'COUNT(*) AS llevado'])->innerJoin('ciclista', 'lleva.dorsal = ciclista.dorsal')->where(['código' => 'MRE'])->groupBy('lleva.dorsal')->asArray()->all();
        $mseData = Lleva::find()->select(['nombre', 'COUNT(*) AS llevado'])->innerJoin('ciclista', 'lleva.dorsal = ciclista.dorsal')->where(['código' => 'MSE'])->groupBy('lleva.dorsal')->asArray()->all();
        $mmsData = Lleva::find()->select(['nombre', 'COUNT(*) AS llevado'])->innerJoin('ciclista', 'lleva.dorsal = ciclista.dorsal')->where(['código' => 'MMS'])->groupBy('lleva.dorsal')->asArray()->all();
        
        return $this->render('maillots', [
            'mgeData' => $mgeData,
            'mmoData' => $mmoData,
            'mmvData' => $mmvData,
            'mreData' => $mreData,
            'mseData' => $mseData,
            'mmsData' => $mmsData,
        ]);
    }
}


